import Company from '../models/company';
import BaseCtrl from './base';

export default class CompanyCtrl extends BaseCtrl {
  model = Company;
}
