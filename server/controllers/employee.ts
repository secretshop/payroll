import Employee from '../models/employee';
import BaseCtrl from './base';

export default class EmplCtrl extends BaseCtrl {
  model = Employee;
}
