import Payslip from '../models/payslip';
import BaseCtrl from './base';

export default class PayslipCtrl extends BaseCtrl {
  model = Payslip;

  getEmpId = (req, res) => {
    this.model.find({employeeId: req.params.id}, {_id:0, payslip:1}, (err, obj) => {
      //this.model.find({}, {_id:0, payslip:1}, (err, obj) => {
      console.log('RECEIVED ----->' + req.params.id);
      if (err) { return console.error(err); }
      res.json(obj);
    });
  };
  

}
