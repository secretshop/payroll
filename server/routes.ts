import * as express from 'express';

import CatCtrl from './controllers/cat';
import CompanyCtrl from './controllers/company';
import UserCtrl from './controllers/user';
import EmplCtrl from './controllers/employee';
import PayslipCtrl from './controllers/payslip';

import Cat from './models/cat';
import Company from './models/company';
import User from './models/user';
import Empl from './models/employee';
import Payslip from './models/payslip';

export default function setRoutes(app) {

  const router = express.Router();

  const catCtrl = new CatCtrl();
  const companyCtrl = new CompanyCtrl();
  const userCtrl = new UserCtrl();
  const emplCtrl = new EmplCtrl();
  const payrollCtrl = new PayslipCtrl();

  // Cats
  router.route('/cats').get(catCtrl.getAll);
  router.route('/cats/count').get(catCtrl.count);
  router.route('/cat').post(catCtrl.insert);
  router.route('/cat/:id').get(catCtrl.get);
  router.route('/cat/:id').put(catCtrl.update);
  router.route('/cat/:id').delete(catCtrl.delete);
  
  // Company
  router.route('/company').get(companyCtrl.getAll);
  router.route('/company/count').get(companyCtrl.count);
  router.route('/company').post(companyCtrl.insert);
  router.route('/company/:id').get(companyCtrl.get);
  router.route('/company/:id').put(companyCtrl.update);
  router.route('/company/:id').delete(companyCtrl.delete);

  // Users
  router.route('/login').post(userCtrl.login);
  router.route('/users').get(userCtrl.getAll);
  router.route('/users/count').get(userCtrl.count);
  router.route('/user').post(userCtrl.insert);
  router.route('/user/:id').get(userCtrl.get);
  router.route('/user/:id').put(userCtrl.update);
  router.route('/user/:id').delete(userCtrl.delete);

  // Employee
  router.route('/employees').get(emplCtrl.getAll);
  router.route('/employees/count').get(emplCtrl.count);
  router.route('/employee').post(emplCtrl.insert);
  router.route('/employee/:id').get(emplCtrl.get);
  router.route('/employee/:id').put(emplCtrl.update);
  router.route('/employee/:id').delete(emplCtrl.delete);

  
  // Payslip
  router.route('/payslip').get(payrollCtrl.getAll);
  router.route('/payslip/count').get(payrollCtrl.count);
  router.route('/payslip').post(payrollCtrl.insert);
  router.route('/payslip/:id').get(payrollCtrl.get);
  router.route('/payslip/emp/:id').get(payrollCtrl.getEmpId);
  //router.route('/payslip/:employeeId').get(payrollCtrl.getEmpId);
  router.route('/payslip/:id').put(payrollCtrl.update);
  router.route('/payslip/:id').delete(payrollCtrl.delete);

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}
