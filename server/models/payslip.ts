import * as mongoose from 'mongoose';

const payslipSchema = new mongoose.Schema({
    employeeId:String,
    companyId:String,
    year:Number,
    payslip:[{
        PayslipDate:Date,
        CreatedDate:{ type: Date, default: Date.now },
        CreatedBy:String,
        ModifiedDate:{ type: Date, default: Date.now },
        ModifiedBy:String,
        IncomeTaxable:{
            Basic:Number, 
            Overtime:Number, 
            Bonus:Number, 
            OtherTaxable:Number},
        LessPreTax:{
            SSS:Number, 
            Philhealth:Number, 
            Pagibig:Number},
        TaxComp:{
            GrossTaxable:Number, 
            WHTax:Number, 
            GrossTaxed:Number}, 
        LessPostTax:{
            Loan:Number, 
            Deduction:Number},
        IncomeNonTaxable:{
            BonusNonTax:Number},
        EmployerShare:{
            EC:Number,
            SSS:Number,
            PHIC:Number,
            HDMF:Number},
        NetPay:Number
    }]
});

const Payslip = mongoose.model('Payslip', payslipSchema);

export default Payslip;