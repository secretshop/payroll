import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
// import { AngularSplitModule } from 'angular-split';
import { ToastComponent } from '../shared/toast/toast.component';

//import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  isLoading = true;
  isEditing = false;
  
  // code tables (dummy).
  genders = [
    {
      code: "M",
      description: "Male"
    },
    {
      code: "F",
      description: "Female"
    }
  ];
  suffixes = [
    {
      code: "Mrs",
      description: "Mrs"
    },
    {
      code: "Mr",
      description: "Mr"
    },
    {
      code: "Ms",
      description: "Ms"
    }
  ];

  employees = []; 
  empl = {};

  constructor(private emplService: EmployeeService,
              private formBuilder: FormBuilder,
              private http: Http,
              public toast: ToastComponent) { }

  ngOnInit() {
     this.getEmployees(); 
     this.isLoading = false;
  }

  getEmployees() {
    this.emplService.getEmployees().subscribe(
      data => this.employees = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  clear() {
    this.empl = {};
  }

  enableEditing(obj, empl) {
    this.isEditing = true;
    this.empl = empl;
    // obj.addClass("active");
    this.isLoading = false;
  }
 
  save(empl) {
    if (typeof empl._id != 'undefined' && empl._id != ""){
      this.updateEmpl(empl);
    }
    else {
      this.addEmpl(empl);
    }
  }

  updateEmpl(empl) {
    this.emplService.updateEmployee(empl).subscribe(
      res => {
        this.isEditing = false;
        this.empl = empl;
        this.getEmployees(); 
        this.toast.setMessage('Item is updated successfully.', 'success');
      },
      error => {
        console.log(error);
        this.toast.setMessage('Error is encountered:' + error, 'error');
      }
    );
  }

  addEmpl(empl) {
    this.emplService.addEmployee(empl).subscribe(
      res => {
        this.isEditing = false;
        this.empl = empl;
        this.getEmployees(); 
        this.toast.setMessage('Item is added successfully.', 'success');
      },
      error => {
        console.log(error);
        this.toast.setMessage('Error is encountered:' + error, 'error');
      }
    );
    this.getEmployees(); 
  }

  cancelEditing() {
    this.isEditing = false;
    // this.empl = {};
    this.toast.setMessage('item editing cancelled.', 'warning');
    // reload the cats to reset the editing
    // this.getEmployees();
  }

  openLoan(empl){

  }

  openPayslip(empl){

  }
}
