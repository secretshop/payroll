import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitpanedemoComponent } from './splitpanedemo.component';

describe('SplitpanedemoComponent', () => {
  let component: SplitpanedemoComponent;
  let fixture: ComponentFixture<SplitpanedemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitpanedemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitpanedemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
