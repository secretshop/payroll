import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AngularSplitModule } from 'angular-split';
//import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { CatService } from '../services/cat.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-splitpanedemo',
  templateUrl: './splitpanedemo.component.html',
  styleUrls: ['./splitpanedemo.component.scss']
})
export class SplitpanedemoComponent implements OnInit {

  isLoading = true;
  isEditing = false;
  cats = [];
  cat = {};
  demo = {
    gender: "",
    dateTime: ""
  }
  genders = [
    {
      code: "M",
      description: "Male"
    },
    {
      code: "F",
      description: "Female"
    }
  ];
  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  addCatForm: FormGroup;
  name = new FormControl('', Validators.required);
  age = new FormControl('', Validators.required);
  weight = new FormControl('', Validators.required);

  constructor(private catService: CatService,
              private formBuilder: FormBuilder,
              public toast: ToastComponent) { }

  ngOnInit() {
    this.getCats();
    this.addCatForm = this.formBuilder.group({
      name: this.name,
      age: this.age,
      weight: this.weight
    });
  }

  getCats() {
    this.catService.getCats().subscribe(
      data => this.cats = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  enableEditing(obj, cat) {
    this.isEditing = true;
    this.cat = cat;
    obj.addClass("active");
  }

  editCat(cat) {
    this.catService.editCat(cat).subscribe(
      res => {
        this.isEditing = false;
        this.cat = cat;
        this.toast.setMessage('Save successful', 'success');
      },
      error => console.log(error)
    );
  }
}
