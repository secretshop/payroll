import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CompanyService {

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  getCompanies(): Observable<any> {
    return this.http.get('/api/company').map(res => res.json());
  }

  countCompany(): Observable<any> {
    return this.http.get('/api/company/count').map(res => res.json());
  }

  addCompany(company): Observable<any> {
    return this.http.post('/api/company', JSON.stringify(company), this.options);
  }

  getCompany(company): Observable<any> {
    return this.http.get(`/api/company/${company._id}`).map(res => res.json());
  }

  editCompany(company): Observable<any> {
    return this.http.put(`/api/company/${company._id}`, JSON.stringify(company), this.options);
  }

  deleteCompany(company): Observable<any> {
    return this.http.delete(`/api/company/${company._id}`, this.options);
  }

}
