import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class PayslipService {

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  getPayslips(): Observable<any> {
    console.log("get all here....");
    return this.http.get('/api/payslip').map(res => res.json());
  }

  countPayslips(): Observable<any> {
    return this.http.get('/api/payslip/count').map(res => res.json());
  }

  addPayslip(payslip): Observable<any> {
    return this.http.post('/api/payslip', JSON.stringify(payslip), this.options);
  }

  getPayslip(payslip): Observable<any> {
    return this.http.get(`/api/payslip/${payslip._id}`).map(res => res.json());
  }
  
  getEmployeePayslip(payslip): Observable<any> {
    //console.log("CHECK EMPLOYEE ID HERE AS ID:  " + payslip.employeeId);
    return this.http.get(`/api/payslip/emp/${payslip.employeeId}`).map(res => res.json());
  }

  editPayslip(payslip): Observable<any> {
    return this.http.put(`/api/payslip/${payslip._id}`, JSON.stringify(payslip), this.options);
  }

  deletePayslip(payslip): Observable<any> {
    return this.http.delete(`/api/payslip/${payslip._id}`, this.options);
  }

}
