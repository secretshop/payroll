import { Component, OnInit } from '@angular/core'; 
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; 
import { FlexLayoutModule } from '@angular/flex-layout'; 
 
import { Http } from '@angular/http'; 
import { PayslipService } from '../services/payslip.service'; 
import { ToastComponent } from '../shared/toast/toast.component'; 

import { PayslipData, PayslipHdr } from './payslip-interface';

//related components are payslip.service.ts, routes.ts, payslip.ts

@Component({ 
  selector: 'app-payslip', 
  templateUrl: './payslip.component.html', 
  styleUrls: ['./payslip.component.scss'] 
}) 
export class PayslipComponent implements OnInit { 
 
  isLoading = true;

  public initialState: PayslipData; 
  public payslipData: PayslipData; 
  public payslipForm: FormGroup; 
  //private sorted: PayslipData;
  

  constructor(private fb: FormBuilder, 
                private payslipService: PayslipService, 
                private http: Http, 
                public toast: ToastComponent) { } 
 
  sometest = {};

  ngOnInit() { 
    //console.log('started...');
    //this.payslipData = this.getPayslip();  
    console.log('calling getPayslipDB');
    this.getPayslipDB();
    console.log('Initial sometest', this.sometest);  
    
    this.payslipData = this.getPayslip();  
    this.payslipForm = this.toFormGroup(this.payslipData);   
    console.log('Initial parentData', this.payslipData);  
  } 
 
  onSubmit(){

  }

  private toFormGroup(data: PayslipData): FormGroup {
    const formGroup = this.fb.group({
        Basic: [data.IncomeTaxable.Basic],
        Overtime: [data.IncomeTaxable.Overtime],
        Bonus: [data.IncomeTaxable.Bonus],
        OtherTaxable: [data.IncomeTaxable.OtherTaxable],
        SSS: [data.LessPreTax.SSS],
        Philhealth: [data.LessPreTax.Philhealth],
        Pagibig: [data.LessPreTax.Pagibig],
        GrossTaxable: [data.TaxComp.GrossTaxable],
        WHTax: [data.TaxComp.WHTax],
        GrossTaxed: [data.TaxComp.GrossTaxed],
        Loan: [data.LessPostTax.Loan],
        Deduction: [data.LessPostTax.Deduction],
        BonusNonTax: [data.IncomeNonTaxable.BonusNonTax],
        NetPay: [data.NetPay]
    });

    return formGroup;
  }
    
  private getPayslip(): PayslipData { 
    return { 
      PayslipDate:new Date('2017-08-21 00:00:00.000'), 
      CreatedDate:new Date('2017-08-21 00:00:00.000'),
      CreatedBy:'Allan',
      ModifiedDate:new Date('2017-08-21 00:00:00.000'),
      ModifiedBy:'Allan',
      IncomeTaxable:{
          Basic:10000, 
          Overtime:0, 
          Bonus:0, 
          OtherTaxable:0},
      LessPreTax:{
          SSS:100, 
          Philhealth:100, 
          Pagibig:100},
      TaxComp:{
          GrossTaxable:10000, 
          WHTax:3000, 
          GrossTaxed:6700}, 
      LessPostTax:{
          Loan:0, 
          Deduction:0},
      IncomeNonTaxable:{
          BonusNonTax:0},
      EmployerShare:{
          EC:100,
          SSS:100,
          PHIC:100,
          HDMF:100},
      NetPay:6700
      // Basic:10000.00, 
      // Overtime:0.00, 
      // Bonus:0.00, 
      // OtherTaxable:0.00, 
      // SSS:100.00, 
      // Philhealth:100.00, 
      // Pagibig:100.00, 
      // GrossTaxable:0.00, 
      // WHTax:0.00, 
      // GrossTaxed:0.00, 
      // Loan:0.00, 
      // Deduction:0.00, 
      // BonusNonTax:0.00, 
      // NetPay:0.00 
    }; 
  }

//TODO: database connection
  getPayslipDB(){
    const bar: PayslipHdr = { 
      employeeId:'12345',
      companyId:'12345',
      year:2017,
      payslip:[]
    };
          //getEmployeePayslip(bar)
    this.payslipService.getPayslips().subscribe(
    //this.payslipService.getEmployeePayslip(bar).subscribe(
      //data => this.sortPayslip(data),
      data => this.sometest = data,
      error => console.log(error),
      () => this.isLoading = false
    );

    console.log('called getPayslipDB after');
  }

  //make an update for array of payslips so that there is a browsable next previous button
  //sortPayslip(data): PayslipData{
    sortPayslip(data){
    //sorted: PayslipData;
    //sorted = data;
    //data.Basic = 20000;
    console.log("checkout data ---->> " + data);
    console.log("checkout payslip ---->> " + data.payslip);
    
    try {
      this.payslipData = data.payslip[0];
      //return data.payslip[0];
    } catch (e) {
      console.log("ERROR MESSAGE:   "); 
      console.log(e instanceof EvalError); // true
      console.log(e.message);              // some Message
      console.log(e.name);                 // "EvalError"
      
      this.payslipData = this.getPayslip(); 
      //return this.getPayslip();
    }
  }

}
 