
export interface IncomeTaxableData{
  Basic:Number, 
  Overtime:Number, 
  Bonus:Number, 
  OtherTaxable:Number
}

export interface LessPreTaxData{
  SSS:Number, 
  Philhealth:Number, 
  Pagibig:Number
}

export interface TaxCompData{
  GrossTaxable:Number, 
  WHTax:Number, 
  GrossTaxed:Number
}

export interface LessPostTaxData{
  Loan:Number, 
  Deduction:Number
}

export interface IncomeNonTaxableData{
  BonusNonTax:Number
}

export interface EmployerShareData{
  EC:Number,
  SSS:Number,
  PHIC:Number,
  HDMF:Number
}

export interface PayslipData { 
    PayslipDate:Date,
    CreatedDate:Date,
    CreatedBy:String,
    ModifiedDate:Date,
    ModifiedBy:String,
    IncomeTaxable:IncomeTaxableData,
    LessPreTax:LessPreTaxData,
    TaxComp:TaxCompData, 
    LessPostTax:LessPostTaxData,
    IncomeNonTaxable:IncomeNonTaxableData,
    EmployerShare:EmployerShareData,
    NetPay:Number

    // Basic:Number, 
    // Overtime:Number, 
    // Bonus:Number, 
    // OtherTaxable:Number, 
    // SSS:Number, 
    // Philhealth:Number, 
    // Pagibig:Number, 
    // GrossTaxable:Number, 
    // WHTax:Number, 
    // GrossTaxed:Number, 
    // Loan:Number, 
    // Deduction:Number, 
    // BonusNonTax:Number, 
    // NetPay:Number 
} 
  
export interface PayslipHdr {
  employeeId:String,
  companyId:String,
  year:Number,
  payslip:PayslipData[]
 }