import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { MdInputModule, MdSelectModule, MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { RoutingModule } from './routing.module';
import { SharedModule } from './shared/shared.module';
import { CatService } from './services/cat.service';
import { CompanyService } from './services/company.service';
import { PayslipService } from './services/payslip.service';
import { UserService } from './services/user.service';
import { EmployeeService } from './services/employee.service';
import { AuthService } from './services/auth.service';
import { AuthGuardLogin } from './services/auth-guard-login.service';
import { AuthGuardAdmin } from './services/auth-guard-admin.service';
import { AngularSplitModule } from 'angular-split';
import { CompanyFormsModule } from './company-forms/company-forms.module';
import { EmployeeModule } from './employee/employee.module';

import { AppComponent } from './app.component';
import { CatsComponent } from './cats/cats.component';
import { AboutComponent } from './about/about.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AccountComponent } from './account/account.component';
import { AdminComponent } from './admin/admin.component';
import { NotFoundComponent } from './not-found/not-found.component';
// import { EmployeeComponent } from './employee/employee.component';
import { SplitpanedemoComponent } from './splitpanedemo/splitpanedemo.component';
import { PayslipComponent } from './payslip/payslip.component';



@NgModule({
  declarations: [
    AppComponent,
    CatsComponent,
    AboutComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    AccountComponent,
    AdminComponent,
    NotFoundComponent,
    // EmployeeComponent,
    SplitpanedemoComponent,
    PayslipComponent
  ],
  imports: [
    RoutingModule,
    SharedModule,
    AngularSplitModule,
    FormsModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdSelectModule,
    MdDatepickerModule,
    MdNativeDateModule,
    CompanyFormsModule, 
    EmployeeModule
  ],
  providers: [
    AuthService,
    AuthGuardLogin,
    AuthGuardAdmin,
    CatService,
    CompanyService,
    UserService,
    PayslipService, 
    EmployeeService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule { }
