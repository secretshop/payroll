import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { CodetableData } from '../company-interface';

@Component({
  selector: 'app-child-list',
  templateUrl: './child-list.component.html',
  styleUrls: ['./child-list.component.scss']
})

export class ChildListComponent implements OnInit {

    @Input('parentForm')
    public parentForm: FormGroup;
    @Input('children')
    public children: CodetableData[];

    // @Input('CodeTable')
    // public CodeTable: CodetableData[];
    // @Input('childrenData')
    // public childrenData: CodetableData[];

    constructor(private cd: ChangeDetectorRef) { }

    ngOnInit() {
        //debugger;
        console.log('Initializing child list', this.children);
        this.parentForm.addControl('children', new FormArray([]));
        // console.log('Initializing child list', this.CodeTable);
        // this.parentForm.addControl('CodeTable', new FormArray([]));
    }

    setChild(){
        //this.CodeTable = ;
        
    }

    addChild() {
        const child: CodetableData = {
            Type:'',
            Code:'',
            Description:'',
            Category:'',
            Rate:''
        };
        // console.log('before add:', this.CodeTable);
        // this.CodeTable = this.CodeTable || [];
        // //debugger;
        // this.CodeTable.push(child);
        // console.log('after add:', this.CodeTable);
        // this.cd.detectChanges();

        this.children.push(child);
        //this.CodeTable.push(child);
        this.cd.detectChanges();
        return false;
    }
    
    removeChild(idx: number) {
        debugger;
        if (this.children.length > 1) {
            this.children.splice(idx, 1);
            (<FormArray>this.parentForm.get('children')).removeAt(idx);
        }
        return false;
    }

    // removeChild(idx: number) {
    //     debugger;
    //     if (this.CodeTable.length > 1) {
    //         this.CodeTable.splice(idx, 1);
    //         (<FormArray>this.parentForm.get('CodeTable')).removeAt(idx);
    //     }
    //     return false;
    // }

}
