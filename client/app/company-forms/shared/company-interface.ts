
export interface CompanyData {
    CompanyName: String,
    Address1: String,
    Address2: String,
    City: String,
    State: String,
    ZIPCode: Number,
    Country: String,
    Industry: String,
    TIN: String,
    SSS: String,
    PhilHealth: String,
    Pagibig: String,
    CodeTable:CodetableData[],
    Loan:CodetableData[],
    Income:CodetableData[],
    Attendance:CodetableData[],
    Overtime:CodetableData[],
    Position:CodetableData[],
    CostCenter:CodetableData[],
    Department:CodetableData[]
}

export interface CodetableData {
    Type:String,
    Code:String,
    Description:String,
    Category:String,
    Rate:String
}