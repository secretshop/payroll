import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CodetableData } from '../company-interface';

//import { MdInputModule } from '@angular/material';
//import { FlexLayoutModule } from '@angular/flex-layout';


@Component({
  selector: 'app-child-form',
  templateUrl: './child-form.component.html',
  styleUrls: ['./child-form.component.scss']
})

export class ChildFormComponent implements OnInit {
    @Input('children')
    public children: FormArray;

    @Input('child')
    public child: CodetableData;

    public childForm: FormGroup;

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
        console.log('Initializing child form', this.child);
        this.childForm = this.toFormGroup(this.child);
        //console.log('Initializing child form CodeTable', this.children);
        // debugger;
        this.children.push(this.childForm);
    }

    private toFormGroup(data: CodetableData) {
        const formGroup = this.fb.group({
            Type:[data.Type],
            Code:[data.Code],
            Description:[data.Description],
            Category:[data.Category],
            Rate:[data.Rate]
        });

        return formGroup;
    }
    

}

