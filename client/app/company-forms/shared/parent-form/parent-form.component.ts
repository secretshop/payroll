import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as _ from 'lodash';

import { CompanyData } from '../company-interface';
import { Http } from '@angular/http';
import { CompanyService } from '../../../services/company.service';
import { ToastComponent } from '../../../shared/toast/toast.component';

//import { MdInputModule } from '@angular/material';
//import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularSplitModule } from 'angular-split';

@Component({
  selector: 'app-parent-form',
  templateUrl: './parent-form.component.html',
  styleUrls: ['./parent-form.component.scss']
})

export class ParentFormComponent implements OnInit, AfterViewInit {

    public initialState: CompanyData;
    public companyData: CompanyData;
    public parentForm: FormGroup;

    constructor(private fb: FormBuilder,
                private companyService: CompanyService,
                private http: Http,
                public toast: ToastComponent) {}

    usercompany = {};
    company = [];
    isLoading = true;
    isEditing = false;

    ngOnInit() {
        this.getCompanies();
        this.initialState = this.getCompanyData();
        this.companyData = _.cloneDeep(this.initialState);
        this.parentForm = this.toFormGroup(this.companyData);
        console.log('Initial parentData', this.companyData);
    }

    ngAfterViewInit() {
        this.parentForm.valueChanges
            .subscribe(value => {
                console.log('Parent Form changed', value);
                this.companyData = _.mergeWith(this.companyData,
                                              value,
                                              this.mergeCustomizer);

            });
    }


    // _.mergeWith customizer to avoid merging primitive arrays, and only
    // merge object arrays
    private mergeCustomizer = (objValue, srcValue) => {
        if (_.isArray(objValue)) {
            if (_.isPlainObject(objValue[0]) || _.isPlainObject(srcValue[0])) {
                return srcValue.map(src => {
                    const obj = _.find(objValue, { id: src.id });
                    return _.mergeWith(obj || {}, src, this.mergeCustomizer);
                });
            }
            return srcValue;
        }
    }

    onSubmit() {
        if (!this.parentForm.valid) {
            console.error('Parent Form invalid, preventing submission');
            return false;
        }

        const updatedCompanyData = _.mergeWith(this.companyData,
                                              this.parentForm.value,
                                              this.mergeCustomizer);

        console.log('Submitting...');
        console.log('Original parentData', this.initialState);
        console.log('Updated parentData', updatedCompanyData);
        if (this.isEditing)
            this.editCompany(updatedCompanyData);
        else
            this.addCompany(updatedCompanyData);
        return false;
    }

    addCompany(updatedCompanyData) {

        this.companyService.addCompany(updatedCompanyData).subscribe( 
        res => { 
            this.parentForm.reset(); 
            this.toast.setMessage('item added successfully.', 'success'); 
        }, 
        error => console.log(error) 
        ); 
    }

    editCompany(updatedCompanyData) {

        this.companyService.editCompany(updatedCompanyData).subscribe( 
        res => { 
            //const newCompany = res.json(); 
            //this.company.push(newCompany); 
            this.isEditing = false;
            this.parentForm.reset(); 
            this.toast.setMessage('item saved successfully.', 'success'); 
        }, 
        error => console.log(error) 
        ); 
    }

    deleteCompany(company) {

        this.companyService.deleteCompany(company).subscribe( 
        res => { 
            this.parentForm.reset(); 
            this.toast.setMessage('item deleted successfully.', 'success'); 
        }, 
        error => console.log(error) 
        ); 
    }


    private getCompanyData(): CompanyData {
        return {
            CompanyName: 'XCompany Name 1',
            Address1: 'XAddress1',
            Address2: 'XAddress2',
            City: 'XNavotas',
            State: 'XMetro Manila',
            ZIPCode: 123,
            Country: 'XPhilippines',
            Industry: 'I.T.',
            TIN: '123',
            SSS: '456',
            PhilHealth: '789',
            Pagibig: '0123',
            CodeTable:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            // Loan:[],
            // Income:[],
            // Attendance:[],
            // Overtime:[],
            // Position:[],
            // CostCenter:[],
            // Department:[]
            Loan:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            Income:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            Attendance:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            Overtime:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            Position:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            CostCenter:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }],
            Department:[{
                 Type:'',
                 Code:'',
                 Description:'',
                 Category:'',
                 Rate:''
            }]
            // [{
            //     Type:'Loan',
            //     Code:'Code1',
            //     Description:'Some Loan',
            //     Category:'Less',
            //     Rate:'1'
            // }, {
            //     Type:'Loan',
            //     Code:'Code2',
            //     Description:'Some Other Loan',
            //     Category:'Less',
            //     Rate:'1'
            // }]
        };
    }    

    private toFormGroup(data: CompanyData): FormGroup {
        const formGroup = this.fb.group({
            CompanyName: [data.CompanyName],
            Address1: [data.Address1],
            Address2: [data.Address2],
            City: [data.City],
            State: [data.State],
            ZIPCode: [data.ZIPCode],
            Country: [data.Country],
            Industry: [data.Industry],
            TIN: [data.TIN],
            SSS: [data.SSS],
            PhilHealth: [data.PhilHealth],
            Pagibig: [data.Pagibig]
        });

        return formGroup;
    }
    

    getCompanies(){
        this.companyService.getCompanies().subscribe(
            data => this.company = data,
            error => console.log(error)
        );

        if (this.company.length > 0)
            this.usercompany = this.company[this.company.length-1];
        // return this.company;
        console.log("getCompanies USER COMPANY: ", this.usercompany);
    } 

    enableEditing(company) {
        this.isEditing = true;
        this.usercompany = company;
        this.companyData = company;
        this.parentForm.reset(this.companyData);
        //this.parentForm.reset({this.toFormGroup(this.companyData)});
        //this.parentForm = this.toFormGroup(this.companyData);
        
        console.log("enable editing: ", this.companyData);
        //console.log("enable editing: " + (this.usercompany  as any).CodeTable[0].Code);
    }


}
