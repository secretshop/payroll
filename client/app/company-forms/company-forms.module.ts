import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyComponent } from './company/company.component';
import { ChildFormComponent } from './shared/child-form/child-form.component';
import { ChildListComponent } from './shared/child-list/child-list.component';
import { ParentFormComponent } from './shared/parent-form/parent-form.component';
import { CompanyFormsRoutingModule } from './company-forms-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularSplitModule } from 'angular-split';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CompanyFormsRoutingModule,
    MdInputModule,
    FlexLayoutModule,
    AngularSplitModule
  ],
  declarations: [CompanyComponent, ChildFormComponent, ChildListComponent, ParentFormComponent]
})
export class CompanyFormsModule { }
